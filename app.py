# coding: utf-8
import random
import time
from flask import Flask, render_template, jsonify

app = Flask(__name__)


@app.route('/')
def index():
    return 'hello world'


@app.route('/test/')
def test():
    start = time.time()
    time.sleep(random.randint(1, 5))
    end = time.time()
    return jsonify(status='ok', time='%.2f seconds' % (end-start), start=start, end=end)


def application(env, start_response):
    start_response('200 OK', [('Content-Type','text/html')])
    return [b"Hello World"]


if __name__ == '__main__':
    app.run(debug=True)
